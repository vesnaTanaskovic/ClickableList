package com.example.android.clickablelistview;


import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView lv1;
    String[] names = { "One", "Two", "Three", "Four", "Five", "Six" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv1 = (ListView) findViewById(R.id.list);
        lv1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names));
        lv1.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent;
        switch (i) {
            case 0:
                intent= new Intent(this,ActivityOne.class);
                startActivity(intent);
                break;
            case 1:
                intent=new Intent(this,ActivityTwo.class);
                startActivity(intent);
                break;
            case 2:
                intent=new Intent(this,ActivityThree.class);
                startActivity(intent);
                break;
            case 3:
                intent=new Intent(this,ActivityFour.class);
                startActivity(intent);
                break;
            case 4:
                intent=new Intent(this,ActivityFive.class);
                startActivity(intent);
                break;
            case 5:
                intent=new Intent(this,ActivitySix.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

}
